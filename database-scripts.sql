CREATE DATABASE noyAdventureTest;

CREATE TABLE Doctors (
	id serial NOT NULL,
	firstname varchar(50) NOT NULL,
	lastname varchar(50) NOT NULL,
	email varchar(25) NOT NULL,
	isactive boolean NULL DEFAULT true,
	CONSTRAINT doctor_pk PRIMARY KEY (id)
);

CREATE TABLE Organization (
	id serial NOT NULL,
	name varchar(50) NOT NULL,
	address varchar(50) NOT NULL,
	CONSTRAINT organization_pk PRIMARY KEY (id)
);

CREATE TABLE DoctorsByOrganization (
	id serial NOT NULL,
	doctor int,
	organization int,
	isactive boolean NULL DEFAULT true,
	CONSTRAINT doctorsOrganization_pk PRIMARY KEY (id),
	CONSTRAINT FK_Doctor FOREIGN KEY (doctor) REFERENCES Doctors(id),
	CONSTRAINT FK_Organization FOREIGN KEY (organization) REFERENCES Organization(id)
);


INSERT INTO Organization (name, address) 
VALUES 
('Administration on Aging.','Address 1'),
('American Academy of Dermatology', 'Address 2'),
('American Academy of Family Physicians', 'Address 3');


INSERT INTO Doctors (firstname, lastname, email)
VALUES 
('Rosario', 'Portillo', 'rportillo@gmail.com'),
('Pablo', 'Orellana', 'porellana1@gmail.com'),
('Marcos', 'Cruz', 'cruzM@gmail.com'),
('Lucia', 'Carballo', 'lcarballo@gmail.com')

INSERT INTO DoctorsByOrganization (doctor, organization)
VALUES 
(1,1),
(2,1),
(3,1)