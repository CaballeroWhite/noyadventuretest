# noyAdventureTest



## Getting started

En el archivo Databasescripts.sql, se encuentran las queries para las tablas, y unos datos de prueba

En el archivo .env.example renombrar por .env

## Comandos Para ejecutar projecto

yarn dev  -> ejecuta el proyecto

## Version de Node

Que se mayor o igual 16.0.0

## Ejecutar con Docker Compose

docker compose up

## Puertos expuestos por Docker compose

Api -> 3001
Database -> 5432

## Instruccion luego de ejecutar con docker compose

Conectarse a la DB y usar los scripts en el archivo SQL, para crear las respectivas tablas