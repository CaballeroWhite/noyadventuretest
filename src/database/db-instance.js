import knex from 'knex'
import knexfile from "./knexfile"

import { ENVITOMENT } from '../config/constants'

const knedEnviroment = ENVITOMENT === 'development' ? knexfile.development : knexfile.production 

const dbInstance = knex(knedEnviroment)

export default dbInstance