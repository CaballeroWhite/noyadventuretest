// Update with your config settings.

import { DATABASE, DBUSERNAME,DBHOST, DBPASSWORD, DBPORT } from '../config/constants'

export default {
  development: {
    client: 'postgresql',
    connection: {
      host: DBHOST,
      port: DBPORT,
      database: DATABASE,
      user:   DBUSERNAME,
      password: DBPASSWORD
    },
    pool: {
      min: 2,
      max: 10
    }
  },
  production: {
    client: 'postgresql',
    connection: {
      host: DBHOST,
      port: DBPORT,
      database: DATABASE,
      user:   DBUSERNAME,
      password: DBPASSWORD
    },
    pool: {
      min: 2,
      max: 10
    }
  }
};
