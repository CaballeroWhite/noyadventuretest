import dbInstance from "../../database/db-instance";

const TableName = "doctors";
const DoctorsOrganizationsTable = "doctorsbyorganization";
const organization = "organization";

const FindAll = async () => {
  const resultOfDoctors = await dbInstance.select("*").orderBy("id", "asc").from(TableName);
  return resultOfDoctors;
};

const FindOne = async (id) => {
  const entity = (
    await dbInstance(TableName)
      .where("doctors.id", id)
      .leftJoin(
        DoctorsOrganizationsTable,
        "doctorsbyorganization.doctor",
        "doctors.id"
      )
      .leftJoin(
        organization,
        "doctorsbyorganization.organization",
        "organization.id"
      )
      .select(
        "doctors.id",
        "doctors.firstname",
        "doctors.lastname",
        "doctors.email",
        "doctors.isactive",
        "organization.id as organizationId",
        "organization.name",
        "organization.address"
      )
  )[0];

  const organizationInfo = entity.organizationId
    ? {
        id: entity.organizationId,
        name: entity.name,
        address: entity.address,
      }
    : null;

  const formatData = {
    id: entity.id,
    firstname: entity.firstname,
    lastname: entity.lastname,
    email: entity.email,
    isactive: entity.isactive,
    organization: organizationInfo,
  };
  return formatData;
};

const SaveDoctor = async (doctorData) => {
  let entity = {};
  const organizationId = doctorData.organization ? doctorData.organization.id : null;
  const payloadToSave = {
    firstname: doctorData.firstname,
    lastname: doctorData.lastname,
    email: doctorData.email,
  };

  await dbInstance.transaction(async (tran) => {
    const result = await tran(TableName).insert(payloadToSave, "*");

    await tran(DoctorsOrganizationsTable).insert({
      doctor: result[0].id,
      organization: organizationId,
    });

    entity = result;
  });

  return entity;
};

const UpdateDoctor = async (id, payload) => {
  const entity = await dbInstance(TableName)
  .where("id", id)
  .update(payload, ["id", "firstname", "lastname", "email"]);
  return entity;
}


export default { FindAll, SaveDoctor, FindOne, UpdateDoctor };
