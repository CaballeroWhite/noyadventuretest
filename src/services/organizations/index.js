import dbInstance from "../../database/db-instance";

const TableName = "organization";
const DoctorsOrganizationsTable = "doctorsbyorganization";
const doctorsTable = "doctors";

const FindAll = async () => {
  const resultOfDoctors = await dbInstance
    .select("*")
    .orderBy("id", "asc")
    .from(TableName);
  return resultOfDoctors;
};

const FindOne = async (id) => {
  const entityResult = (await dbInstance.where({ id }).from(TableName))[0];
  return entityResult;
};

const Save = async (payload) => {
  const entity = await dbInstance(TableName).insert(payload, "*");
  return entity;
};

const Update = async (id, payload) => {
  const entity = await dbInstance(TableName)
    .where("id", id)
    .update(payload, ["id", "name", "address"]);
  return entity;
};

const findDoctor = async (id) => {
  const entity = await dbInstance(doctorsTable).where("id", id).select('*')
  return entity
}

const ListOfDoctorsByOrganization = async (organizationId, filters) => {
  const formatData = []
  const result = await dbInstance(TableName)
    .where(`${TableName}.id`, organizationId)
    .where(`${DoctorsOrganizationsTable}.isactive`, filters.isactive)
    .innerJoin(
      DoctorsOrganizationsTable,
      `${DoctorsOrganizationsTable}.organization`,
      `${TableName}.id`
    );
  
  for(const entity of result) {
    const doctorInformation = (await findDoctor(entity.doctor))[0]

    formatData.push({
      id: doctorInformation.id,
      firstname: doctorInformation.firstname,
      lastname: doctorInformation.lastname,
      email: doctorInformation.email,
      isactive: doctorInformation.isactive
    })
  }

  return formatData;
};

const addDoctorToOrganization = async (organizationId, doctorId) => {
  const result = await dbInstance(DoctorsOrganizationsTable).insert(
    {
      doctor: doctorId,
      organization: organizationId,
    },
    "*"
  );

  return result;
};

const removeDoctorFromOrganization = async (organizationId, doctorId) => {
  const result = await dbInstance(DoctorsOrganizationsTable).where('organization', Number(organizationId)).where('doctor', doctorId).delete()
  return result;
};

export default {
  FindAll,
  FindOne,
  Save,
  Update,
  ListOfDoctorsByOrganization,
  addDoctorToOrganization,
  removeDoctorFromOrganization,
};
