const FormatResponse = (value, status) => {
  return { message: value, status };
};

const RequireDataProperty = () => FormatResponse("Need Data property", 400);
const SanitizeResponse = (responseData) => {
  return { data: responseData };
};

const pagination = (data, page = 1, pageSize = 15) => {
  const limit = pageSize
  const starPaginationIndex = (page - 1) * limit
  const endPaginationIndex = page * limit

  const paginateData = data.slice(starPaginationIndex, endPaginationIndex)
  
  return { ...SanitizeResponse(paginateData), meta: { page, pageSize, pageCount: paginateData.length, total: data.length }}
}

export { FormatResponse, RequireDataProperty, SanitizeResponse, pagination };
