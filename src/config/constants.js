export const APP_PORT = process.env.VITE_APP_PORT

// DB 
export const DATABASE = process.env.VITE_DB_DATABASE
export const DBPASSWORD = process.env.VITE_DB_PASSWORD
export const DBUSERNAME = process.env.VITE_DB_USERNAME
export const DBHOST = process.env.VITE_DB_HOST
export const DBPORT = process.env.VITE_DB_PORT

export const ENVITOMENT = process.env.ENVITOMENT