import cors from '@fastify/cors'
import Fastify from 'fastify'

import Routes from './controllers/index.js'
// config file
import { APP_PORT } from './config/constants'

const app = Fastify({ logger: false })
// set app config
app.register(cors, {})
app.get('/', (_request, reply) => {
  reply.send('Doctors API')
})

Routes.forEach((route, _index) => {
  app.route(route)
})

app.listen({ port: APP_PORT }, (error) => {
  if (error)
    app.log.error(error)
})

export const viteNodeApp = app