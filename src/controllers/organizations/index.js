// Doctor Controller Fastify Instance
import OrganizationService from '../../services/organizations/index.js'
import { RequireDataProperty, SanitizeResponse, pagination } from '../../utils/Http/Form-Response.js'

const FindAll = async (req, reply) => {
  const result = await OrganizationService.FindAll()
  return reply.send(result)
}

const FindOne = async (req, reply) => {
  const id = req.params.id
  const result = await OrganizationService.FindOne(id)
  return reply.send(SanitizeResponse(result))
}

const Save = async (req, reply) => {
  const payload = req.body.data
  
  // validate incoming Data
  if (!req.body.data)
    return reply.code(400).send(RequireDataProperty())

  const entity = await OrganizationService.Save(payload)
 
  return reply.send(SanitizeResponse(entity))
}

const Update = async (req, reply) => {
  const id = req.params.id
  const payload = req.body.data

  if (!req.body.data)
    return reply.code(400).send(RequireDataProperty())
  
  const entity = await OrganizationService.Update(id, payload)

  return entity
}

const AddDoctor = async (req, reply) => {
  const body = req.body.data
  const id = req.params.id

  if (!req.body.data)
    return reply.code(400).send(RequireDataProperty())
  
  const entity = await OrganizationService.addDoctorToOrganization(id, body.doctor.id)
  return reply.send(SanitizeResponse(entity))
}

const RemoveDoctor = async (req, reply) => {
  const body = req.body.data
  const id = req.params.id

  if (!req.body.data)
    return reply.code(400).send(RequireDataProperty())
  
  await OrganizationService.removeDoctorFromOrganization(id, body.doctor.id)
  return reply.send("Success")
}


const CollectionOfDoctorsByOrganization = async (req, reply) => {
  const id = req.params.id
  const { isactive, page, pageSize } = req.query

  const filters = {
    isactive: isactive ?? true,
  }
  
  const results = await OrganizationService.ListOfDoctorsByOrganization(id, filters)

  return reply.send(pagination(results, Number(page), Number(pageSize)))
}

export default { FindAll, FindOne, Save,Update, CollectionOfDoctorsByOrganization, AddDoctor, RemoveDoctor }