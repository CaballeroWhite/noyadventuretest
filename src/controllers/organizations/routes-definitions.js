import Controllers from './index.js'


const routes = [
  {
    method: 'GET',
    url: '/organizations',
    handler: Controllers.FindAll
  },
  {
    method: 'GET',
    url: '/organizations/:id',
    handler: Controllers.FindOne
  },
  {
    method: 'GET',
    url: '/organizations/:id/doctors',
    handler: Controllers.CollectionOfDoctorsByOrganization
  },
  {
    method: 'POST',
    url: '/organizations',
    handler: Controllers.Save
  },
  {
    method: 'POST',
    url: '/organizations/:id/add-doctor',
    handler: Controllers.AddDoctor
  },
  {
    method: 'DELETE',
    url: '/organizations/:id/remove-doctor',
    handler: Controllers.RemoveDoctor
  },
  {
    method: 'PUT',
    url: '/organizations/:id',
    handler: Controllers.Update
  }
]


export default routes