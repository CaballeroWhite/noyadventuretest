import DoctorRoutes from './doctors/routes-definitions'
import OrganizationRoutes from './organizations/routes-definitions'


const unifiedRoutes = [...DoctorRoutes, ...OrganizationRoutes]

export default unifiedRoutes

