// Doctor Controller Fastify Instance
import DoctorService from "../../services/doctors/index.js";
import {
  RequireDataProperty,
  SanitizeResponse,
  pagination,
} from "../../utils/Http/Form-Response.js";

const FindAll = async (req, reply) => {
  const { page, pageSize } = req.query;
  const result = await DoctorService.FindAll();
  const paginated = pagination(result, page, pageSize);
  return reply.send(paginated);
};

const FindOne = async (req, reply) => {
  const id = req.params.id;
  const result = await DoctorService.FindOne(id);
  return reply.send(SanitizeResponse(result));
};

const Save = async (req, reply) => {
  // validate incoming Data
  if (!req.body.data) return reply.code(400).send(RequireDataProperty());

  const payload = req.body.data;
  const entity = await DoctorService.SaveDoctor(payload);

  return reply.send(SanitizeResponse(entity[0]));
};

const Update = async (req, reply) => {
  // validate incoming Data
  if (!req.body.data) return reply.code(400).send(RequireDataProperty());

  const payload = req.body.data;
  const id = req.params.id
  const entity = await DoctorService.UpdateDoctor(id, payload);
  return reply.send(SanitizeResponse(entity[0]))
};
export default { FindAll, FindOne, Save, Update };
