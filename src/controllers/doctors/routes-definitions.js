import Controllers from './index.js'


const routes = [
  {
    method: 'GET',
    url: '/doctors',
    handler: Controllers.FindAll
  },
  {
    method: 'GET',
    url: '/doctors/:id',
    handler: Controllers.FindOne
  },
  {
    method: 'POST',
    url: '/doctors',
    handler: Controllers.Save
  },
  {
    method: 'PUT',
    url: '/doctors/:id',
    handler: Controllers.Update
  }
]


export default routes