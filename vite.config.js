import { defineConfig, loadEnv } from 'vite'
import { VitePluginNode } from 'vite-plugin-node'

function source(mode) {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }
}

export default defineConfig(({ mode }) => {
  source(mode)

  return {
    // ...vite configures
    server: {
      port: Number.parseInt(process.env.VITE_APP_PORT || '7071', 10),
    },
    plugins: [
      ...VitePluginNode({
        adapter: 'fastify',
        appPath: './src/app.js',
        exportName: 'viteNodeApp',
      }),
    ],
  }
})
